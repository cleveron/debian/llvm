llvm (1:19.0.0~20240616-1) unstable; urgency=medium

  * Git 3904aca886d2.
  * Drop upstreamed patches.
  * Drop libc++ grow_by() re-enablement as relevant software has been
    recompiled.
  * Add patch to fix compiling lldb with P2996.

 -- Mairi Dubik <mairi.dubik@clevon.com>  Sun, 16 Jun 2024 13:22:06 +0300

llvm (1:19.0.0~20240529-1) unstable; urgency=medium

  * Git b539b289a58c.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 29 May 2024 15:10:01 +0300

llvm (1:19.0.0~20240520-1) unstable; urgency=medium

  * Git dc634446c3aa.
  * Ship the BOLT post-link optimizer.
  * Install libc++ modules.
  * Install Clang runtime libraries into /usr/lib/${DEB_HOST_MULTIARCH} instead
    of /usr/lib/${DEB_HOST_MULTIARCH}/clang.
  * Fix llvm-config returning incorrect non-multiarch paths.
  * Update libc++ patches from PRs.
  * Add patch to fix incorrect -Wcast-function-type-mismatch warnings.
  * Fix LLDB Python module library symlink making it unloadable.
  * Switch back to setting the default standard level using config files.
    Meson will be patched to address why this was done like this previously.
    This fixes most of the test suite.
  * Adjust tests to match downstream patches.
  * Hone patches to require less test changes.
  * Simplify various patches to reduce potential future conflicts.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 20 May 2024 19:14:47 +0300

llvm (1:19.0.0~20240402-1) unstable; urgency=medium

  * Git ae9c3493bd25.
  * Build from Bloomberg's P2996 implementation.
  * Fix runtimes not being built with the base toolchain, again.
  * Build tests if nocheck build profile hasn't been specified.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 02 Apr 2024 16:17:38 +0300

llvm (1:19.0.0~20240401-1) unstable; urgency=medium

  * Git a7206a6fa32a.
  * Fix runtimes not being built with the base toolchain.
  * Correct the bug report URL.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 01 Apr 2024 16:50:28 +0300

llvm (1:19.0.0~20240319-1) unstable; urgency=medium

  * Git 66125ad8e997.
  * Default arm64 architecture to ARMv8.2-A.
  * Use the vendor string from dpkg.
  * Fix CUDA debug builds with __PRETTY_FUNCTION__.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 19 Mar 2024 17:37:26 +0200

llvm (1:19.0.0~20240310-1) unstable; urgency=medium

  * Git 193b3d6733b7.
  * Pass -D_TIME_BITS=64 and -D_FILE_OFFSET_BITS=64 by default.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 10 Mar 2024 22:30:09 +0200

llvm (1:19.0.0~20240306-1) unstable; urgency=medium

  * Git 6e27dd47e1fc.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 06 Mar 2024 11:15:18 +0200

llvm (1:19.0.0~20240219-1) unstable; urgency=medium

  * Git 6d7de46155fe.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 19 Feb 2024 11:01:49 +0200

llvm (1:19.0.0~20240129-1) unstable; urgency=medium

  * Git 754a8add5709.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 29 Jan 2024 15:29:04 +0200

llvm (1:18.0.0~20240117-1) unstable; urgency=medium

  * Git 103fa3250c46.
  * Mark build-essential-clang as Multi-Arch: foreign.
    Clang is able to directly target any architecture unlike GCC.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 17 Jan 2024 22:20:49 +0200

llvm (1:18.0.0~20240102-1) unstable; urgency=medium

  * Git 5842dfe34d89.
  * Drop patch for std::ranges::contains() as it's been merged upstream.
  * Disable Intel MPX support for LLDB as it's obsolete hardware-wise.
  * Force the use of Debian's default Python version.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 02 Jan 2024 15:11:04 +0200

llvm (1:18.0.0~20231219-1) unstable; urgency=medium

  * Git 87bf1afbbf03.
  * Add patch for std::ranges::find_last() (P1223R5).

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 19 Dec 2023 11:00:17 +0200

llvm (1:18.0.0~20231211-1) unstable; urgency=medium

  * Git 2460bf2facd1.
  * Add patch to fix CUDA compatibility with libc++.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 11 Dec 2023 14:20:26 +0200

llvm (1:18.0.0~20231205-1) unstable; urgency=medium

  * Git 600c12987e17.
  * Default debuginfod to Debian's server if DEBUGINFOD_URLS is not set.
  * Add a Conflicts on libc++abi-17-dev (>= 1:17.0.6-1) to libc++-dev due to
    Debian bug #1057448.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 05 Dec 2023 14:52:59 +0200

llvm (1:18.0.0~20231103-1) unstable; urgency=medium

  * Git e9db60c05e2f.
  * Add patch for ranges::contains (P2302R4) from #70258.
  * Drop patch for ranges::slide_view as it's broken.

 -- Raul Tambre <raul.tambre@clevon.com>  Fri, 03 Nov 2023 11:43:32 +0200

llvm (1:18.0.0~20231024-1) unstable; urgency=medium

  * Git 7bc1031c474e.
  * Add patch for ranges::slide_view from #67146.
  * Update P2116R9 patch.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 24 Oct 2023 12:24:11 +0300

llvm (1:18.0.0~20231011-1) unstable; urgency=medium

  * Git 85d0fbeaae46.
  * Add patch for P2116R9 from D157193.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 11 Oct 2023 19:22:58 +0300

llvm (1:18.0.0~20231010-1) unstable; urgency=medium

  * Git 12a4757b0fa2.
  * Support the nodoc build profile.
  * Patch to fix a rare std::atomic<T*>::wait() hang.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 10 Oct 2023 17:57:38 +0300

llvm (1:18.0.0~20230822-1) unstable; urgency=medium

  * Git 048b50683934.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 22 Aug 2023 13:27:09 +0300

llvm (1:18.0.0~20230815-1) unstable; urgency=medium

  * Git d60c3d08e78d.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 15 Aug 2023 11:17:25 +0300

llvm (1:18.0.0~20230807-1) unstable; urgency=medium

  * Git f2bdc29f3e5d.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 07 Aug 2023 13:43:29 +0300

llvm (1:17.0.0~20230527-1) unstable; urgency=medium

  * Git 96b59b4f0655.
  * Bump the default C++ standard to 26.

 -- Raul Tambre <raul.tambre@clevon.com>  Sat, 27 May 2023 20:16:21 +0300

llvm (1:17.0.0~20230502-1) unstable; urgency=medium

  * Git c7de29e7bb66.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 02 May 2023 21:41:34 +0300

llvm (1:17.0.0~20230413-1) unstable; urgency=medium

  * Git 1fb24cef40d2.
  * Cleanup and rename patches.
  * Simplify install files by installing straight into the triple directory.
  * Mark various Clang library packages Multi-Arch: same.
  * Merge libclang-examples into libclang-dev, fix it being empty.
  * Fix LLDB Python extension.

 -- Raul Tambre <raul.tambre@clevon.com>  Thu, 13 Apr 2023 19:56:15 +0300

llvm (1:17.0.0~20230322-1) unstable; urgency=medium

  * Git c2df1d8a6d1c.
  * Revert to old Clang language default mechanism due to an edge case with the
    preprocessor.
  * Combine triple-related patches.

 -- Raul Tambre <raul.tambre@clevon.com>  Thu, 23 Mar 2023 08:03:18 +0200

llvm (1:17.0.0~20230126-1) unstable; urgency=medium

  * Git ef4628c66e04.
  * Fix C++23 default not working.
  * Use Debian triple for Clang configs search.

 -- Raul Tambre <raul.tambre@clevon.com>  Thu, 26 Jan 2023 17:50:24 +0200

llvm (1:17.0.0~20230125-1) unstable; urgency=medium

  * Git 8184b563a42d.
  * Fix defaulting to C23 and C++23.
  * Default x86 architecture to Skylake.
  * Use Debian-style triples for Clang config file search.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 25 Jan 2023 12:08:24 +0200

llvm (1:16.0.0~20230122-1) unstable; urgency=medium

  * Git 3c560dd0ae67 (Closes: #11).
  * Build gold LTO plugin (Closes: #9).
  * FunctionSpecialization pass enabled upstream (Closes: #8).
  * Drop libcxx-notify_all-mutex.patch as it has been applied upstream.
  * Allow tests, support DEB_BUILD_OPTIONS=nocheck.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 22 Jan 2023 17:26:46 +0200

llvm (1:16.0.0~20220821-1) unstable; urgency=medium

  * Git e6a0800532bb.
  * Move to more upstream-conformant versioning scheme.
  * Don't statically link libc++ to reduce binary size. The performance
    difference should be negligible.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 21 Aug 2022 15:26:14 +0300

llvm (1:15.0.0+20220717-1) unstable; urgency=medium

  * Git 26ce33706f8f.
  * Fix broken CMake configs due to wrong installation path calculations.
  * Move Clang runtimes into architecture library directories.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 17 Jul 2022 15:49:18 +0300

llvm (1:15.0.0+20220624-1) unstable; urgency=medium

  * Git 8c278a27811c.

 -- Raul Tambre <raul.tambre@clevon.com>  Fri, 24 Jun 2022 17:40:32 +0300

llvm (1:15.0.0+20220608-1) unstable; urgency=medium

  * Git bf21cda7f260.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 08 Jun 2022 14:50:42 +0300

llvm (1:15.0.0+20220525-1) unstable; urgency=medium

  * Git 4baae166ce33.
  * Patch to fix building compiler-rt profile library with compiler extensions
    off by default.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 25 May 2022 21:21:11 +0300

llvm (1:15.0.0+20220508-1) unstable; urgency=medium

  * Git 4d1fd705f0b0.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 08 May 2022 20:38:17 +0300

llvm (1:15.0.0+20220423-1) unstable; urgency=medium

  * Git 7a98d8351b27.
  * Disable GNU extensions by default (Closes: #6).

 -- Raul Tambre <raul.tambre@clevon.com>  Sat, 23 Apr 2022 19:40:28 +0300

llvm (1:15.0.0+20220410-1) unstable; urgency=medium

  * Git aed0e8b8056a.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 10 Apr 2022 16:45:42 +0300

llvm (1:15.0.0+20220403-1) unstable; urgency=medium

  * Git b48abeea44ac (Closes: #1).
  * Keep patches previously applied onto the tree as Debian patches.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 03 Apr 2022 13:13:39 +0300

llvm (1:15.0.0+20220318-1) unstable; urgency=medium

  * Remove now spurious plus in the versioning scheme.
  * Remove Provides/Conflicts/Replaces from libc++2 to allow side-by-side soversion installation.
  * Remove Provides/Conflicts/Replaces from libpolly as it isn't shipped in upstream.

 -- Raul Tambre <raul@tambre.ee>  Fri, 18 Mar 2022 12:58:03 +0200

llvm (1:15.0.0++20220318-1) unstable; urgency=medium

  * Git eacce1614e04 (Closes: #3).
  * Fix tools Python 3 support (Closes: #4).
  * Disable various unwanted tools to reduce size.
  * Bump libc++ soversion.

 -- Raul Tambre <raul@tambre.ee>  Fri, 18 Mar 2022 11:27:29 +0200

llvm (1:14.0.0++20220116-1) unstable; urgency=medium

  * Git 5f8d44e510ed.
  * Refresh patches.

 -- Raul Tambre <raul@tambre.ee>  Sun, 16 Jan 2022 18:59:46 +0200

llvm (1:14.0.0++20211209-1) unstable; urgency=medium

  * Git 6e4f4a3b7c1a.
  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Thu, 09 Dec 2021 13:32:39 +0200

llvm (1:14.0.0++20211122-1) unstable; urgency=medium

  * Git 1adf275d1530.
  * Update upstream.
  * Fix python3-lldb SO extensions.

 -- Raul Tambre <raul@tambre.ee>  Mon, 22 Nov 2021 11:20:19 +0200

llvm (1:14.0.0++20211114-1) unstable; urgency=medium

  * Git 1ec90ac07da7.
  * Update upstream.
  * Apply D103395 diff 4.
  * Unapply D106779.

 -- Raul Tambre <raul@tambre.ee>  Sun, 14 Nov 2021 16:52:39 +0200

llvm (1:14.0.0++20211104-1) unstable; urgency=medium

  * Git eed9cdd57505.
  * Update upstream.
  * Apply D108482.

 -- Raul Tambre <raul@tambre.ee>  Thu, 04 Nov 2021 11:43:31 +0200

llvm (1:14.0.0++20211030-1) unstable; urgency=medium

  * Git d0f3c289fd3b.
  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sat, 30 Oct 2021 21:22:55 +0300

llvm (1:14.0.0++20211029-1) unstable; urgency=medium

  * Git 14bcc69917fc.
  * Update upstream.
  * Bump Standards-Version to 4.6.0.
  * Improve package descriptions.
  * Add missing copyright licence bodies.
  * Cleanup some depends.

 -- Raul Tambre <raul@tambre.ee>  Fri, 29 Oct 2021 16:19:37 +0300

llvm (1:14.0.0+git20211024.0c7c0c3d9d42-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sun, 24 Oct 2021 16:05:36 +0300

llvm (1:14.0.0+git20211007.e47316cd93c9-1) unstable; urgency=medium

  * Update upstream.
  * For older APU2.

 -- Raul Tambre <raul@tambre.ee>  Thu, 07 Oct 2021 17:49:26 +0300

llvm (1:14.0.0+git20211002.eb70ba4b8731-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sat, 02 Oct 2021 14:34:38 +0300

llvm (1:14.0.0+git20210919.1cccb5528a8d-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sun, 19 Sep 2021 17:16:50 +0300

llvm (1:14.0.0+git20210821.d546c9aa1fc9-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sat, 21 Aug 2021 14:47:50 +0300

llvm (1:13.0.0+git20210720.1791bb779c7a-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Tue, 20 Jul 2021 11:36:36 +0300

llvm (1:13.0.0+git20210718.3ea65b7177df-1) unstable; urgency=medium

  * Update upstream.
  * Re-enabled -fno-semantic-interposition.

 -- Raul Tambre <raul@tambre.ee>  Sun, 18 Jul 2021 17:51:34 +0300

llvm (1:13.0.0+git20210626.84f675a900e9-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sat, 26 Jun 2021 13:47:58 +0300

llvm (1:13.0.0+git20210625.b57a64d0e05a-1) unstable; urgency=medium

  * Update upstream.
  * Remove upstreamed SO symbolic flags.
  * Fix missing libclangrt shared library dependencies.

 -- Raul Tambre <raul@tambre.ee>  Fri, 25 Jun 2021 22:08:35 +0300

llvm (1:13.0.0+git20210503.c55007e53e1a-1) unstable; urgency=medium

  * Update upstream.
  * Refresh patches.
  * Update bug report URL.
  * Don't statically link Polly into libLLVM SO.
  * Add build-essential-clang as an alternative to the GNU GCC based.
  * Bump Standards-Version to 4.5.1, no changes required.
  * Add various missing dependencies.
  * Pass necessary options for non-native builtin targets.
  * Patch triples to match Debian.
  * Bind symbols to same shared object to improve performance
    (-Bsymbolic-functions).

 -- Raul Tambre <raul@tambre.ee>  Mon, 03 May 2021 10:45:10 +0300

llvm (1:13.0.0+git20210304.958edda33113-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Wed, 03 Mar 2021 13:18:22 +0200

llvm (1:13.0.0+git20210127.8417d1cba166-1) unstable; urgency=medium

  * Update upstream.
  * Add python3-sphinx-automodapi to Build-Dpeends.

 -- Raul Tambre <raul@tambre.ee>  Wed, 27 Jan 2021 10:57:24 +0200

llvm (1:12.0.0+git20201215.29840b3d8129-1) unstable; urgency=medium

  * Update upstream.
  * IR PGO VP counters override.

 -- Raul Tambre <raul@tambre.ee>  Tue, 15 Dec 2020 08:46:12 +0200

llvm (1:12.0.0+git20201214.12469d030671-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Mon, 14 Dec 2020 17:05:30 +0200

llvm (1:12.0.0+git20201213.6022dfecf168-1) unstable; urgency=medium

  * Update upstream.
  * Remove lldb dependency on llvm-dev. Seems unnecessary.
  * Strip RPATH when installing.

 -- Raul Tambre <raul@tambre.ee>  Sun, 13 Dec 2020 14:01:32 +0200

llvm (1:12.0.0+git20201205.6efcfaa6d10-1) unstable; urgency=medium

  * Update upstream.
  * Switch default C++ language mode to 23.

 -- Raul Tambre <raul@tambre.ee>  Sat, 05 Dec 2020 09:31:48 +0200

llvm (1:12.0.0+git20201123.f10967992d8-1) unstable; urgency=medium

  * Update upstream.
  * Drop tilde from nightly version.

 -- Raul Tambre <raul@tambre.ee>  Mon, 23 Nov 2020 12:24:18 +0200

llvm (1:12.0.0~+git20201109.fa5d31f8256-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Mon, 09 Nov 2020 08:43:40 +0200

llvm (1:12.0.0~+git20201020.dcd7f3d0344-1) unstable; urgency=medium

  * Update upstream.
  * Add clang depends on the native builtin libraries.
  * Bootstrap llvm-tblgen.
  * Enable ARM code generation support.
  * Build armv7em-none-eabi builtin libraries.
  * Define operator new and delete in libc++abi as preferred by upstream.
  * Don't include version control information in version string.

 -- Raul Tambre <raul@tambre.ee>  Tue, 20 Oct 2020 08:16:28 +0300

llvm (1:12.0.0~+git20200913.ab5ecd2a-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sun, 13 Sep 2020 07:04:06 +0300

llvm (1:12.0.0~+git20200910.019cc8a6-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Thu, 10 Sep 2020 11:50:37 +0300

llvm (1:12.0.0~+git20200909.e67ece79-1) unstable; urgency=medium

  * Correctly pass options to runtimes build.

 -- Raul Tambre <raul@tambre.ee>  Wed, 09 Sep 2020 07:38:53 +0300

llvm (1:12.0.0~+git20200909.b536f542-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Wed, 09 Sep 2020 06:25:20 +0300

llvm (1:12.0.0~+git20200908.524f890d-1) unstable; urgency=medium

  * Split runtime libraries into separate Architecture:all packages. Makes
    crosscompiling using compiler-rt possible.
  * Disable Z3 when crosscompiling. LLVM's CMake code doesn't work with it.
  * Require libpython3-dev to fix LLDB bindings when crosscompiling.

 -- Raul Tambre <raul@tambre.ee>  Tue, 08 Sep 2020 10:51:18 +0300

llvm (1:12.0.0~+git20200907.5e295009-1) unstable; urgency=medium

  * Update upstream.
  * ARM64 crosscompiling fixes.
  * Don't require python3-dev. Only the interpreter is necessary.

 -- Raul Tambre <raul@tambre.ee>  Mon, 07 Sep 2020 16:46:17 +0300

llvm (1:12.0.0~+git20200905.7f0dc0c0-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Sat, 05 Sep 2020 09:53:12 +0300

llvm (1:12.0.0~+git20200903.7d0696da-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul@tambre.ee>  Thu, 03 Sep 2020 08:24:17 +0300

llvm (1:12.0.0~+git20200902.5db26d80-1) unstable; urgency=medium

  * Instrumented build support.
  * Add Clang to update-alternatives.
  * Change default language standards to C20 and C++20.

 -- Raul Tambre <raul@tambre.ee>  Wed, 02 Sep 2020 11:48:48 +0300

llvm (1:12.0.0~+git20200902.8d1b3b05-1) unstable; urgency=medium

  * Initial release.

 -- Raul Tambre <raul@tambre.ee>  Tue, 01 Sep 2020 16:07:05 +0300
